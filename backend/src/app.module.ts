import { Module, NestModule } from '@nestjs/common';
import {
  APP_FILTER,
  APP_GUARD,
  APP_INTERCEPTOR,
  MiddlewareBuilder,
} from '@nestjs/core';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { HttpExceptionFilter } from './common/filters/http-exception.fiter';
import { TimeoutInterceptor } from './common/interceptors/timout.interceptor';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmModuleOptions } from './config/typeorm';
import { AuthModule } from './auth/auth.module';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { TransformInterceptor } from './interceptors/transform.interceptor';
import { VersionMiddleware } from './common/middlewares/version.middleware';
import { BullModule } from '@nestjs/bull';
import { MailModule } from './common/mail/mail.module';
import { HealthCheckModule } from './modules/healthcheck/helth-check.module';
import { TokensModule } from './modules/v1/token/token.module';
import { BullOptions } from './config/bull';
import { ConfigGlobal } from './config/configGlobal';
import { ProductsModule } from './modules/v1/products/product.module';
import { UsersModule } from './modules/v1/users/users.module';
import { TradeMarksModule } from './modules/v1/trademark/trademark.module';
import { CategoriesModule } from './modules/v1/categories/category.module';
import { PermissionsModule } from './modules/v1/permission/permission.module';
import { FileUploadModule } from './modules/v1/files/file.module';

const commonModule = [HealthCheckModule, AuthModule, MailModule];

const moduleV1 = [
  FileUploadModule,
  UsersModule,
  PermissionsModule,
  ProductsModule,
  TradeMarksModule,
  CategoriesModule,
  TokensModule,
];

@Module({
  imports: [
    ConfigModule.forRoot(ConfigGlobal),
    TypeOrmModule.forRootAsync(TypeOrmModuleOptions),
    BullModule.forRootAsync(BullOptions),
    ConfigModule.forRoot({ isGlobal: true }),
    ThrottlerModule.forRoot({ ttl: 60, limit: 10000 }),
    ...commonModule,
    ...moduleV1,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor,
    },
    // {
    //   provide: APP_FILTER,
    //   useClass: HttpExceptionFilter,
    // },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  public configure(consumer: MiddlewareBuilder): void {
    consumer.apply(VersionMiddleware).forRoutes('*');
  }
}
