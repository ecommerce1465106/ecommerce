import { Module } from '@nestjs/common';
import { MinioModule } from 'nestjs-minio-client';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MinioClientService } from './minio.service';
@Module({
  imports: [
    MinioModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          endPoint: configService.get<string>('minio.end_point'),
          port: configService.get<number>('minio.port'),
          useSSL: false,
          accessKey: configService.get<string>('minio.access_key'),
          secretKey: configService.get<string>('minio.secret_key'),
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [MinioClientService],
  exports: [MinioClientService],
})
export class MinioClientModule {}
