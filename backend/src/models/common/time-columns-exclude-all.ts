import { Exclude } from 'class-transformer';
import {
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  BaseEntity,
  Column,
} from 'typeorm';

export abstract class TimeColumnsExclude extends BaseEntity {
  @Column({ nullable: true })
  @Exclude()
  public readonly createdById!: number;

  @Column({ nullable: true })
  @Exclude()
  public readonly updatedById!: number;

  @Column({ nullable: true })
  @Exclude()
  public readonly deletedById!: number;

  @CreateDateColumn()
  @Exclude()
  public readonly createdAt!: Date;

  @UpdateDateColumn()
  @Exclude()
  public readonly updatedAt!: Date;

  @DeleteDateColumn()
  @Exclude()
  public readonly deletedAt!: Date;
}
