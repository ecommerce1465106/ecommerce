import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TokenEntity } from '@root/modules/v1/token/entities/token.entity';
import { TokensService } from '@root/modules/v1/token/token.service';
import { Request, Response } from 'express';
import { IsNull } from 'typeorm';

@Injectable()
export class VersionMiddleware implements NestMiddleware {
  constructor(
    private readonly tokenService: TokensService,
    private readonly configService: ConfigService,
  ) {}

  async use(req: Request, res: Response, next: () => void) {
    if (req?.headers['authorization']) {
      const token = req?.headers['authorization'].split(' ')[1];

      if (!token) {
        throw new UnauthorizedException();
      }

      const data: TokenEntity = await this.tokenService.getToken(token);

      if (
        req?.headers['user-agent'] !== data?.userAgent ||
        req?.socket?.remoteAddress !== data?.ipAddress
      ) {
        await Promise.all([
          this.tokenService.updateToken(
            { id: data?.id, deletedAt: IsNull() },
            { isBlackList: true },
          ),
          this.tokenService.updateToken(
            { ipAddress: data?.ipAddress, deletedAt: IsNull() },
            { isBlackList: true },
          ),
        ]);

        throw new UnauthorizedException();
      }

      if (!data && !data?.token && !data?.isBlackList) {
        throw new UnauthorizedException();
      }

      req.headers['authorization'] = `${this.configService.get<string>(
        'jwt.token_type',
      )} ${data.token}`;

      next();
    } else {
      next();
    }
  }
}
