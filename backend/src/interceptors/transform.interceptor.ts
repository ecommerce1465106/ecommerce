import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface ListResponse<T> extends ExtendedResponse<T> {
  result: any[];
}

export const calcListTotalCount = (
  data = [],
  total: number,
  page: number,
  limit: number,
) => {
  const totalPages =
    total % limit === 0 ? total / limit : Math.floor(total / limit) + 1;

  const hasPrevPage = page <= 1 ? false : true;
  const hasNextPage = page < totalPages ? true : false;
  const prevPage = page <= 1 ? null : page - 1;
  const nextPage = page < totalPages ? page + 1 : null;

  return {
    data,
    totalDocs: total,
    limit,
    totalPages,
    page,
    hasPrevPage,
    hasNextPage,
    prevPage,
    nextPage,
  };
};

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, ListResponse<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<ListResponse<T>> {
    const request: Request = context.switchToHttp().getRequest();

    const pageQr = parseInt(request.query['page'] as string, 10);
    const limitQr = parseInt(request.query['limit'] as string, 10);

    return next.handle().pipe(
      map((value) => {
        if (value instanceof Object && value?.data?.length >= 0) {
          const {
            data,
            totalDocs,
            limit,
            totalPages,
            page,
            hasPrevPage,
            hasNextPage,
            prevPage,
            nextPage,
          } = calcListTotalCount(value.data, value.total, pageQr, limitQr);

          return {
            data,
            totalDocs,
            limit,
            totalPages,
            page,
            hasPrevPage,
            hasNextPage,
            prevPage,
            nextPage,
          };
        } else if (!!pageQr && !!limitQr && !value.total) {
          return {
            data: [],
            totalDocs: 0,
            limit: limitQr,
            totalPages: 0,
            page: pageQr,
            hasPrevPage: false,
            hasNextPage: false,
            prevPage: null,
            nextPage: null,
          };
        }

        return value;
      }),
    );
  }
}
