import { registerAs } from '@nestjs/config';

export default registerAs('minio', () => ({
  end_point: process.env.MINIO_ENDPOINT,
  port: +process.env.MINIO_PORT,
  access_key: process.env.MINIO_ACCESSKEY,
  secret_key: process.env.MINIO_SECRETKEY,
  bucket: process.env.MINIO_BUCKET,
}));
