import { registerAs } from '@nestjs/config';

export default registerAs('database', () => ({
  host: process.env.DEVELOPMENT_DB_HOST,
  port: +process.env.DEVELOPMENT_DB_PORT,
  username: process.env.DEVELOPMENT_DB_USERNAME,
  password: process.env.DEVELOPMENT_DB_PASSWORD,
  name: process.env.DEVELOPMENT_DB_DATABASE,
}));
