import appConfig from '../environments/app.config';
import auth0Config from '../environments/auth0.config';
import databaseConfig from '../environments/database.config';
import elasticsearchConfig from '../environments/elasticsearch.config';
import jwtConfig from '../environments/jwt.config';
import minioConfig from '../environments/minio.config';
import redisConfig from '../environments/redis.config';

export const ConfigGlobal = {
  isGlobal: true,
  load: [
    databaseConfig,
    jwtConfig,
    redisConfig,
    appConfig,
    auth0Config,
    elasticsearchConfig,
    minioConfig,
  ],
  validationOptions: {},
  envFilePath: '.env',
};
