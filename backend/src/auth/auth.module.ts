import { JwtStrategy } from './strategies/jwt.strategy';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from '@root/modules/v1/users/users.module';
import { UserEntity } from '@root/modules/v1/users/entities/user.entity';
import { TokensService } from '@root/modules/v1/token/token.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TokenEntity } from '@root/modules/v1/token/entities/token.entity';
import { RedisService } from '@root/common/utils/cache.util';
import { GoogleStrategy } from './strategies/google.strategy';
import { FacebookStrategy } from './strategies/facebook.strategy';

@Module({
  imports: [
    PassportModule,
    PassportModule.register({ defaultStrategy: 'google' }),
    PassportModule.register({ name: 'facebook', strategy: FacebookStrategy }),
    TypeOrmModule.forFeature([UserEntity, TokenEntity]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          secret: configService.get('jwt.access_token_secret'),
          signOptions: {
            expiresIn: `${configService.get('jwt.access_token_ttl')}s`,
          },
        };
      },
      inject: [ConfigService],
    }),
    UsersModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    TokensService,
    JwtService,
    JwtStrategy,
    GoogleStrategy,
    RedisService,
    FacebookStrategy,
  ],
  exports: [AuthService],
})
export class AuthModule {}
