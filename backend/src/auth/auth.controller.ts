import { ClassSerializerInterceptor } from '@nestjs/common/serializer';
import {
  Controller,
  Post,
  Body,
  HttpCode,
  Req,
  Res,
  Get,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RegisterUserDto } from '@root/auth/dto/create-user.dto';
import { AuthService } from './auth.service';
import { LoginUserDto } from '@root/auth/dto/login-user.dto';
import { Request, Response } from 'express';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Auth')
@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('register')
  @HttpCode(201)
  public async register(@Body() registerUserDto: RegisterUserDto) {
    return await this.authService.register(registerUserDto);
  }

  @Post('login')
  @HttpCode(201)
  public async login(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
    @Body() loginUserDto: LoginUserDto,
  ) {
    const ipClient = req?.socket?.remoteAddress;
    const userAgent = req?.headers['user-agent'];

    const data = await this.authService.login(
      loginUserDto,
      ipClient,
      userAgent,
    );

    return data;
  }

  @Post('refresh-token')
  @HttpCode(201)
  public async refreshToken(@Req() req: Request) {
    const token = req?.cookies['token'];

    console.log('header: ', req.headers);
    console.log('token: ', token);

    const ipAddress = req?.socket?.remoteAddress;
    const userAgent = req?.headers['user-agent'];

    return await this.authService.refreshToken(token, ipAddress, userAgent);
  }

  @Get('google')
  @UseGuards(AuthGuard('google'))
  public googleLogin() {
    return;
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  public googleLoginCallback(@Req() req: Request) {
    return req.user;
  }

  @Get('facebook')
  @UseGuards(AuthGuard('facebook'))
  facebookLogin() {
    return;
  }

  @Get('facebook/callback')
  @UseGuards(AuthGuard('facebook'))
  facebookLoginCallback(@Req() req: Request) {
    return req.user;
  }
}
