import { Role } from '@root/config/enum/role.enum';

export interface ILoginResult {
  access_token: string;
  expiredAt: string;
  refresh_token: string;
  user: {
    id: number;
    email: string;
    profileImage: string;
    fullName: string;
    role: Role;
  };
}
