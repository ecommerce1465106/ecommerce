import { errorMessage } from './../config/constant/error-message';
import { Injectable, HttpException } from '@nestjs/common';
import { RegisterUserDto } from '@root/auth/dto/create-user.dto';
import { UsersService } from '@root/modules/v1/users/users.service';
import { hashPassword, isValidPassword } from '@root/common/utils/cipher.util';
import { Role } from '@root/config/enum/role.enum';
import { LoginUserDto } from '@root/auth/dto/login-user.dto';
import { UserEntity } from '@root/modules/v1/users/entities/user.entity';
import { ILoginResult } from '@root/auth/interfaces/login-result.interface';
import { TokensService } from '@root/modules/v1/token/token.service';
import { IJwtPayload } from '@root/modules/v1/token/interfaces/jwt.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly tokensService: TokensService,
  ) {}

  public async register(registerUserDto: RegisterUserDto): Promise<UserEntity> {
    const { fullName, email, phone, password, confirmPassword } =
      registerUserDto;

    if (password !== confirmPassword) {
      throw new HttpException(errorMessage.PasswordNotMatch, 400);
    }

    const [checkEmail, checkPhone] = await Promise.all([
      this.usersService.findUser({ email }),
      this.usersService.findUser({ phone }),
    ]);

    if (checkEmail) {
      throw new HttpException(errorMessage.EmailIsExist, 400);
    } else if (checkPhone) {
      throw new HttpException(errorMessage.PhoneIsExist, 400);
    }

    const hashPass = await hashPassword(password, 10);

    const newUser = await this.usersService.createUser({
      fullName,
      email,
      phone,
      password: hashPass,
      role: Role.user,
    });

    delete newUser.password;

    return newUser;
  }

  public async login(
    loginUserDto: LoginUserDto,
    ipClient: string,
    userAgent: string,
  ): Promise<ILoginResult> {
    const { account, password } = loginUserDto;

    const user: any = await this.usersService.findUserByEmailOrPhone(account);

    if (!user) {
      throw new HttpException(errorMessage.UserNotExist, 401);
    }

    if (!isValidPassword(password, user.password)) {
      throw new HttpException(errorMessage.PasswordNotMatch, 401);
    }

    user.permissions = user.permissions.map((item: any) => {
      console.log('name: ', item.name);
      return {
        name: item.name,
      };
    });

    const payload: IJwtPayload = {
      email: user.email,
      id: user.id,
      role: user.role,
      permissions: user.permissions,
    };

    const [access_token, token_expired_time] =
      this.tokensService.signAccessToken(payload);
    const [refresh_token] = this.tokensService.signRefreshToken(payload);

    const data = await this.tokensService.createToken(
      access_token,
      refresh_token,
      token_expired_time,
      user.id,
      ipClient,
      userAgent,
    );

    return {
      access_token: data.token,
      expiredAt: token_expired_time.toISOString(),
      refresh_token: data.refreshToken,
      user: {
        id: user.id,
        email: user.email,
        profileImage: user.profileImage,
        fullName: user.fullName,
        role: user.role,
      },
    };
  }

  public async refreshToken(
    token: string,
    ipAddress: string,
    userAgent: string,
  ) {
    return await this.tokensService.refreshToken(token, ipAddress, userAgent);
  }
}
