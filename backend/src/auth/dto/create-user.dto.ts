import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsPhoneNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Gender } from '@root/config/enum/gender.enum';

export class RegisterUserDto {
  @ApiProperty()
  @IsString()
  @MinLength(1)
  @MaxLength(50)
  fullName: string;

  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  @MinLength(8)
  password: string;

  @ApiProperty()
  @MinLength(8)
  confirmPassword: string;

  @ApiProperty()
  @IsEnum(Gender)
  gender: Gender;
}
