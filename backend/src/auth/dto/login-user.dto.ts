import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

export class LoginUserDto {
  @ApiProperty()
  account: string;

  @ApiProperty()
  @IsString()
  @MinLength(8)
  password: string;
}
