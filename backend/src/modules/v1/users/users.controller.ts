import { ClassSerializerInterceptor } from '@nestjs/common/serializer';
import { Controller, Get } from '@nestjs/common';
import { UseInterceptors } from '@nestjs/common/decorators';
import { ApiTags } from '@nestjs/swagger';
import { UserEntity } from './entities/user.entity';
import { UsersService } from './users.service';
import { Auth } from '@root/decorators/auth.decorator';

@Auth()
@ApiTags('User')
@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  public async getListUser(): Promise<UserEntity[]> {
    return await this.usersService.getListUser();
  }
}
