import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RedisService } from '@root/common/utils/cache.util';
import { Repository } from 'typeorm';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,

    private readonly redisService: RedisService,
  ) {}

  public async createUser(data: object): Promise<UserEntity> {
    return this.usersRepository.save(data);
  }

  public async findUser(filter: object): Promise<UserEntity> {
    return await this.usersRepository.findOne({
      where: [{ ...filter, isBlock: false }],
    });
  }

  public async findUserByEmailOrPhone(data: string): Promise<UserEntity> {
    return await this.usersRepository.findOne({
      where: [{ email: data }, { phone: data }],
      relations: ['permissions'],
    });
  }

  public async getListUser(): Promise<UserEntity[]> {
    return await this.redisService.getOrSet<UserEntity[]>(
      'list_user',
      async () => {
        return await this.usersRepository.find({
          relations: ['tokens'],
        });
      },
      500,
    );
  }
}
