import { Role } from '@root/config/enum/role.enum';
import { IsNotEmptyString } from '@root/decorators/is-not-empty-string.decorator';
import { IsOptionalBoolean } from '@root/decorators/is-optional-boolean.decorator';
import { TimeColumns } from '@root/models/common/time-columns';
import { IsEmail } from 'class-validator';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { TokenEntity } from '@root/modules/v1/token/entities/token.entity';
import { PermissionEntity } from '../../permission/entities/permission.entity';
import { Exclude } from 'class-transformer';

@Entity('users')
export class UserEntity extends TimeColumns {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @IsNotEmptyString(1, 50)
  @Column({ length: 50, nullable: false, select: true })
  public fullName!: string;

  @Column({ nullable: true, select: true })
  public profileImage!: string;

  @IsNotEmptyString(1, 50)
  @Column({ nullable: true, unique: true, select: true })
  public phone!: string;

  @IsEmail()
  @IsNotEmptyString(1, 50)
  @Column({ nullable: true, unique: true, select: true })
  public email!: string;

  @Column({ select: true })
  @Exclude()
  password: string;

  @IsOptionalBoolean()
  @Column({ nullable: true, width: 1, select: true })
  public gender!: boolean;

  @IsOptionalBoolean()
  @Column({ width: 1, nullable: false, select: false, default: false })
  @Exclude()
  public smsAdsConsent!: boolean;

  @IsOptionalBoolean()
  @Column({ width: 1, nullable: false, select: false, default: false })
  @Exclude()
  public emailAdsConsent!: boolean;

  @Column({ enum: Role, default: Role.user })
  @Exclude()
  public role: Role;

  @Column({ default: false })
  @Exclude()
  isBlock: boolean;

  @OneToMany(() => TokenEntity, (token) => token.user)
  @Exclude()
  tokens: TokenEntity[];

  @OneToMany(() => PermissionEntity, (permission) => permission.user)
  @Exclude()
  permissions: PermissionEntity[];
}
