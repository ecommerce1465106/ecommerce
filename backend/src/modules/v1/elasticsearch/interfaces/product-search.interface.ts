import { ProductCategoriesEntity } from '../../categories/entities/product-categories.entity';
import { ProductTradeMarksEntity } from '../../trademark/entities/product-trademark.entity';

export interface ProductSearchBody {
  id: number;
  title: string;
  slug: string;
  desc: string;
  supplier: string;
  category: ProductCategoriesEntity;
  trademark: ProductTradeMarksEntity;
}

export interface ProductSearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: ProductSearchBody;
    }>;
  };
}
