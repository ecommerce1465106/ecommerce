import { ApiProperty } from '@nestjs/swagger';
import { Permissions } from '@root/config/enum/permission.enum';
import { Type } from 'class-transformer';
import { IsArray, IsNumber } from 'class-validator';

export class CreatePermissionDto {
  @ApiProperty()
  @IsArray()
  permissions: Permissions[];

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  userId: number;
}
