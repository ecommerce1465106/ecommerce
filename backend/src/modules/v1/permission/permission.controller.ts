import { CreatePermissionDto } from './dto/create-permission.dto';
import { PermissionEntity } from './entities/permission.entity';
import { Controller, Get, Body, Post, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { PermissionsService } from './permission.service';
import { ReqUser } from '@root/decorators/req-user.decorator';
import { Permissions } from '@root/config/enum/permission.enum';
import { Auth } from '@root/decorators/auth.decorator';
import { JwtAuthGuard } from '@root/guards/jwt-auth.guard';

@ApiTags('Permissions')
@Controller('permissions')
export class PermissionsController {
  constructor(private readonly permissionsService: PermissionsService) {}

  // @Auth(Permissions.getPermissions)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get()
  public async getPermissions(): Promise<any> {
    return await this.permissionsService.getPermissions();
  }

  // @Auth(Permissions.createPermission)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Post()
  public async createPermission(
    @ReqUser() userId: number,
    @Body() createPermissionDto: CreatePermissionDto,
  ): Promise<PermissionEntity[]> {
    return await this.permissionsService.createPermission(
      userId,
      createPermissionDto,
    );
  }
}
