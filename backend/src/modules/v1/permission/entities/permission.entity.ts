import { TimeColumnsExclude } from '@root/models/common/time-columns-exclude-all';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Unique,
} from 'typeorm';
import { UserEntity } from '../../users/entities/user.entity';

@Entity('permissions')
@Unique(['userId', 'name'])
export class PermissionEntity extends TimeColumnsExclude {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @Column()
  name: string;

  @ManyToOne(() => UserEntity, (user) => user.permissions)
  user: UserEntity[];
}
