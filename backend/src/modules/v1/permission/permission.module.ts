import { UserEntity } from '@root/modules/v1/users/entities/user.entity';
import { PermissionEntity } from './entities/permission.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisService } from '@root/common/utils/cache.util';
import { PermissionsController } from './permission.controller';
import { PermissionsService } from './permission.service';
import { UsersService } from '../users/users.service';

@Module({
  imports: [TypeOrmModule.forFeature([PermissionEntity, UserEntity])],
  controllers: [PermissionsController],
  providers: [PermissionsService, RedisService, UsersService],
  exports: [PermissionsService],
})
export class PermissionsModule {}
