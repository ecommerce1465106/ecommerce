import { errorMessage } from './../../../config/constant/error-message';
import { Injectable, Body, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Permissions,
  permissionsName,
} from '@root/config/enum/permission.enum';
import { ReqUser } from '@root/decorators/req-user.decorator';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { PermissionEntity } from './entities/permission.entity';

@Injectable()
export class PermissionsService {
  constructor(
    @InjectRepository(PermissionEntity)
    private readonly permissionRepository: Repository<PermissionEntity>,

    private readonly userService: UsersService,
  ) {}

  public async getPermissions() {
    const permissionsArray = Object.values(Permissions);

    const result = permissionsName.map((name) => {
      return {
        [name]: permissionsArray.filter((permission) =>
          permission.includes(name),
        ),
      };
    });

    return result;
  }

  public async createPermission(
    userId: number,
    createPermissionDto: CreatePermissionDto,
  ) {
    const user = await this.userService.findUser({
      id: userId,
    });

    if (!user) throw new HttpException(errorMessage.UserNotExist, 400);

    let data = [];

    data = createPermissionDto.permissions.map((permission) => {
      return {
        name: permission,
        userId: user.id,
        createdById: userId,
      };
    });

    return await this.permissionRepository.save(data);
  }
}
