import { Injectable } from '@nestjs/common';
import { BufferedFile } from '@root/minio/file.model';
import { MinioClientService } from '@root/minio/minio.service';

@Injectable()
export class FileUploadService {
  constructor(private minioClientService: MinioClientService) {}

  async uploadSingle(image: BufferedFile) {
    const uploaded_image = await this.minioClientService.upload(image);

    return {
      url: uploaded_image.url,
    };
  }
}
