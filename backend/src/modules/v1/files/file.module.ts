import { Module } from '@nestjs/common';
import { MinioClientModule } from '@root/minio/minio.module';
import { FileUploadController } from './file.controller';
import { FileUploadService } from './file.service';

@Module({
  imports: [MinioClientModule],
  controllers: [FileUploadController],
  providers: [FileUploadService],
})
export class FileUploadModule {}
