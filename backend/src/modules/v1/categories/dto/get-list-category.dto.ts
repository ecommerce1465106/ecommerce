import { ApiProperty } from '@nestjs/swagger';
import { PaginateDto } from '@root/common/dto/paginate.dto';

export class GetListCategory extends PaginateDto {}
