import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisService } from '@root/common/utils/cache.util';
import { CategoriesController } from './category.controller';
import { CategoriesService } from './category.service';
import { ProductCategoriesEntity } from './entities/product-categories.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProductCategoriesEntity])],
  controllers: [CategoriesController],
  providers: [CategoriesService, RedisService],
  exports: [CategoriesService],
})
export class CategoriesModule {}
