import { RedisService } from '@root/common/utils/cache.util';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, IsNull, Like } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { ProductCategoriesEntity } from './entities/product-categories.entity';
import { CreateCategoryDto } from './dto/create-category.dto';
import { GetListCategory } from './dto/get-list-category.dto';
import { Order } from '@root/config/enum/order-by.enum';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(ProductCategoriesEntity)
    private readonly categoryRepository: Repository<ProductCategoriesEntity>,

    private readonly redisService: RedisService,
  ) {}

  public async createCategory(
    userId: number,
    createCategoryDto: CreateCategoryDto,
  ): Promise<ProductCategoriesEntity> {
    const { ...data } = createCategoryDto;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [_, trademark] = await Promise.all([
      this.redisService.del('categories'),
      this.categoryRepository.save({
        ...data,
        createdById: userId,
      }),
    ]);

    return trademark;
  }

  public async getListCategory(getListCategory: GetListCategory): Promise<any> {
    const { page, limit, search } = getListCategory;
    const [data, total] = await this.redisService.getOrSet(
      `categories ${JSON.stringify(getListCategory)}`,
      async () => {
        const [data, total] = await Promise.all([
          this.categoryRepository.find({
            where: [
              search
                ? { deletedAt: IsNull(), name: Like(`%${search}%`) }
                : { deletedAt: IsNull() },
            ],
            order: {
              startTotal: Order.DESC,
            },
            skip: (page - 1) * limit,
            take: limit,
          }),
          this.categoryRepository.count({
            where: [
              search
                ? { deletedAt: IsNull(), name: Like(`%${search}%`) }
                : { deletedAt: IsNull() },
            ],
          }),
        ]);

        return [data, total];
      },
      1000,
    );

    return { data, total };
  }

  public async getCategoryById(
    categoryId: number,
  ): Promise<ProductCategoriesEntity> {
    return await this.redisService.getOrSet(
      `${categoryId}`,
      async () => {
        return await this.categoryRepository.findOne({
          where: [{ id: categoryId, deletedAt: IsNull() }],
        });
      },
      1000,
    );
  }
}
