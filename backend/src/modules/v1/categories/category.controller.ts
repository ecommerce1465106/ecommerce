import { Controller, Get, Body, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Permissions } from '@root/config/enum/permission.enum';
import { ReqUser } from '@root/decorators/req-user.decorator';
import { CategoriesService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { ProductCategoriesEntity } from './entities/product-categories.entity';
import { Auth } from '@root/decorators/auth.decorator';
import { UseGuards } from '@nestjs/common/decorators';
import { JwtAuthGuard } from '@root/guards/jwt-auth.guard';
import { GetListCategory } from './dto/get-list-category.dto';

@ApiTags('Categories')
@Controller('category')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  // @Auth(Permissions.createCategory)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Post()
  public async createCategory(
    @ReqUser() userId: number,
    @Body() categoriesService: CreateCategoryDto,
  ): Promise<ProductCategoriesEntity> {
    return await this.categoriesService.createCategory(
      userId,
      categoriesService,
    );
  }

  @Get()
  public async getCategories(
    @Query() getListCategory: GetListCategory,
  ): Promise<ProductCategoriesEntity[]> {
    return await this.categoriesService.getListCategory(getListCategory);
  }
}
