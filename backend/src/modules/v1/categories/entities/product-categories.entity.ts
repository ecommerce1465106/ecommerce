import { TimeColumns } from '@root/models/common/time-columns';
import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ProductEntity } from '../../products/entities/product.entity';

@Entity('categories')
export class ProductCategoriesEntity extends TimeColumns {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 0 })
  startTotal: number;

  @OneToMany(() => ProductEntity, (product) => product.category)
  products: ProductEntity[];
}
