import { Logger } from '@nestjs/common';
import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueFailed,
  Process,
  Processor,
} from '@nestjs/bull';
import { Job } from 'bull';
import { JobName } from '@root/config/enum/job-name.enum';
import { ProductsSearchService } from './productSearch.service';
import { QueueName } from '@root/config/enum/queue-name.enum';
import { CategoriesService } from '../categories/category.service';
import { TradeMarksService } from '../trademark/trademark.service';

@Processor(QueueName.createProduct)
export class ProductsProcessor {
  private readonly logger = new Logger(this.constructor.name);

  constructor(
    private readonly productSearchService: ProductsSearchService,
    private readonly categoriesService: CategoriesService,
    private readonly trademarkService: TradeMarksService,
  ) {}

  @OnQueueActive()
  onActive(job: Job) {
    console.log(
      `Processor:@OnQueueActive - Processing job ${job.id} of type ${
        job.name
      }. Data: ${JSON.stringify(job.data)}`,
    );
  }

  @OnQueueCompleted()
  onComplete(job: Job) {
    console.log(
      `Processor:@OnQueueCompleted - Completed job ${job.id} of type ${job.name}.`,
    );
  }

  @OnQueueFailed()
  onError(job: Job<any>, error) {
    console.log(
      `Processor:@OnQueueFailed - Failed job ${job.id} of type ${job.name}: ${error.message}`,
      error.stack,
    );
  }

  @Process(JobName.CreateProduct)
  async addProductElasticsearch(job: Job): Promise<any> {
    console.log('Processor:@Process - Creating product.');

    try {
      const { categoryId, tradeMarkId } = job.data;

      if (categoryId) {
        const category = await this.categoriesService.getCategoryById(
          categoryId,
        );

        job.data.category = category.name;
      }

      if (tradeMarkId) {
        const tradeMark = await this.trademarkService.getTradeMarkById(
          tradeMarkId,
        );

        job.data.tradeMark = tradeMark.name;
      }

      console.log('data: ', job.data);

      await this.productSearchService.indexProduct(job.data);
    } catch (error) {
      this.logger.error('Failed to add product elasticsearch.', error.stack);
      throw error;
    }
  }
}
