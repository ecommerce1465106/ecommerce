export interface IProduct {
  id: number;
  title: string;
  slug: string;
  desc: string;
  category: string;
  supplier: string;
  tradeMark: string;
}
