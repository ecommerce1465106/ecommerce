import { Controller, Get, Body, Post, Query, Param } from '@nestjs/common';
import {
  Delete,
  HttpCode,
  Patch,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common/decorators';
import { ClassSerializerInterceptor } from '@nestjs/common/serializer';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Permissions } from '@root/config/enum/permission.enum';
import { Auth } from '@root/decorators/auth.decorator';
import { ReqUser } from '@root/decorators/req-user.decorator';
import { JwtAuthGuard } from '@root/guards/jwt-auth.guard';
import { CreateProductDto } from './dto/create-product.dto';
import { GetListProductDto } from './dto/get-list-product.dto';
import { GetProductDto } from './dto/get-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ProductEntity } from './entities/product.entity';
import { ProductsService } from './product.service';

@ApiTags('Products')
@Controller('products')
@UseInterceptors(ClassSerializerInterceptor)
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  // @Auth(Permissions.createProduct)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Post()
  public async createProduct(
    @ReqUser() userId: number,
    @Body() createProductDto: CreateProductDto,
  ): Promise<ProductEntity> {
    console.log({ userId });
    return await this.productsService.createProduct(userId, createProductDto);
  }

  @Get()
  public async searchEngineListProduct(
    @Query() getListProductDto: GetListProductDto,
  ) {
    return await this.productsService.searchEngineListProduct(
      getListProductDto,
    );
  }

  // @Get()
  // public async getListProduct(
  //   @Query() getListProductDto: GetListProductDto,
  // ): Promise<ProductEntity[]> {
  //   return await this.productsService.getListProduct(getListProductDto);
  // }

  @Get(':productId')
  public async getProductById(
    @Param() getProductDto: GetProductDto,
  ): Promise<ProductEntity> {
    return await this.productsService.getProductById(getProductDto.productId);
  }

  // @Auth(Permissions.updateProduct)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Patch(':productId')
  public async updateProduct(
    @ReqUser() userId: number,
    @Param() getProductDto: GetProductDto,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    return await this.productsService.updateProductById(
      userId,
      getProductDto.productId,
      updateProductDto,
    );
  }

  // @Auth(Permissions.deleteProduct)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Delete(':productId')
  @HttpCode(204)
  public async deleteProduct(
    @ReqUser() userId: number,
    @Param() getProductDto: GetProductDto,
  ) {
    await this.productsService.deleteProduct(userId, getProductDto.productId);

    return {};
  }
}
