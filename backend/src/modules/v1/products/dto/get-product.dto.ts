import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class GetProductDto {
  @ApiProperty()
  @Type(() => Number)
  @IsNumber()
  productId: number;
}
