import { ApiProperty } from '@nestjs/swagger';
import { Supplier } from '@root/config/enum/supplier.enum';
import {
  IsArray,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class Media {
  @ApiProperty()
  @IsString()
  imageUrl: string;
}

export class Property {
  @ApiProperty()
  @IsString()
  p_k: string;

  @ApiProperty()
  @IsString()
  p_v: string;
}

export class CreateProductDto {
  @ApiProperty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsString()
  desc: string;

  @ApiProperty()
  @IsNumber()
  amount: number;

  @ApiProperty()
  @IsString()
  thumnail: string;

  @ApiProperty()
  @IsString()
  shortLink: string;

  @ApiProperty()
  @IsString()
  rootLink: string;

  @ApiProperty()
  @IsEnum(Supplier)
  supplier: Supplier;

  @ApiProperty()
  @IsArray()
  medias: Media[];

  @ApiProperty()
  @IsArray()
  properties: Property[];

  @ApiProperty({ required: false })
  @IsNumber()
  @IsOptional()
  categoryId: number;

  @ApiProperty({ required: false })
  @IsNumber()
  @IsOptional()
  tradeMarkId: number;
}
