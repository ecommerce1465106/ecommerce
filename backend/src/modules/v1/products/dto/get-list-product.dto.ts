import { ApiProperty } from '@nestjs/swagger';
import { PaginateDto } from '@root/common/dto/paginate.dto';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export class GetListProductDto extends PaginateDto {
  @ApiProperty({ required: false })
  @IsNumber()
  @Type(() => Number)
  @IsOptional()
  categoryId?: number;

  @ApiProperty({ required: false })
  @IsNumber()
  @Type(() => Number)
  @IsOptional()
  tradeMarkId?: number;
}
