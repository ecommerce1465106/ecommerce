import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisService } from '@root/common/utils/cache.util';
import { QueueName } from '@root/config/enum/queue-name.enum';
import { CategoriesService } from '../categories/category.service';
import { ProductCategoriesEntity } from '../categories/entities/product-categories.entity';
import { ElasticSearchModule } from '../elasticsearch/elasticsearch.module';
import { ProductTradeMarksEntity } from '../trademark/entities/product-trademark.entity';
import { TradeMarksService } from '../trademark/trademark.service';
import { ProductMediaEntity } from './entities/product-media.entity';
import { ProductPropertiesEntity } from './entities/product-properties.entity';
import { ProductEntity } from './entities/product.entity';
import { ProductsController } from './product.controller';
import { ProductsProcessor } from './product.process';
import { ProductsService } from './product.service';
import { ProductsSearchService } from './productSearch.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProductEntity,
      ProductMediaEntity,
      ProductPropertiesEntity,
      ProductCategoriesEntity,
      ProductTradeMarksEntity,
    ]),
    ElasticSearchModule,
    BullModule.registerQueueAsync({
      name: QueueName.createProduct, // mail queue name
      useFactory: () => ({
        redis: {
          host: process.env.REDIS_HOST,
          port: Number(process.env.REDIS_PORT),
        },
      }),
    }),
  ],
  controllers: [ProductsController],
  providers: [
    ProductsService,
    RedisService,
    ProductsSearchService,
    CategoriesService,
    TradeMarksService,
    ProductsProcessor,
  ],
  exports: [ProductsService],
})
export class ProductsModule {}
