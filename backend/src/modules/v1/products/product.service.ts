import { ProductsSearchService } from './productSearch.service';
import { RedisService } from '@root/common/utils/cache.util';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Like, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ProductEntity } from './entities/product.entity';
import { CreateProductDto } from './dto/create-product.dto';
import { ProductMediaEntity } from './entities/product-media.entity';
import { ProductPropertiesEntity } from './entities/product-properties.entity';
import { toSlug } from '@root/common/helpers/slug.helper';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectQueue } from '@nestjs/bull';
import { QueueName } from '@root/config/enum/queue-name.enum';
import { Queue } from 'bull';
import { JobName } from '@root/config/enum/job-name.enum';
import { GetListProductDto } from './dto/get-list-product.dto';
import { calcListTotalCount } from '@root/interceptors/transform.interceptor';
import { Order } from '@root/config/enum/order-by.enum';

@Injectable()
export class ProductsService {
  constructor(
    @InjectQueue(QueueName.createProduct)
    private productQueue: Queue,

    @InjectRepository(ProductEntity)
    private readonly productsRepository: Repository<ProductEntity>,

    private readonly redisService: RedisService,

    private readonly productsSearchService: ProductsSearchService,
  ) {}

  public async createProduct(
    userId: number,
    createProductDto: CreateProductDto,
  ): Promise<ProductEntity> {
    // eslint-disable-next-line prefer-const
    let { medias, properties, ...data } = createProductDto;

    const manager = this.productsRepository.manager;

    let newProduct: ProductEntity;

    await manager.transaction(async (transactionalEntityManager) => {
      newProduct = await transactionalEntityManager
        .getRepository(ProductEntity)
        .save({
          ...data,
          userId,
          slug: toSlug(data.title),
        });

      medias = medias.map((item: any) => {
        return {
          ...item,
          productId: newProduct.id,
        };
      });

      properties = properties.map((item: any) => {
        return {
          ...item,
          productId: newProduct.id,
        };
      });

      await Promise.all([
        transactionalEntityManager
          .getRepository(ProductMediaEntity)
          .insert(medias),
        transactionalEntityManager
          .getRepository(ProductPropertiesEntity)
          .insert(properties),
      ]);
    });

    if (newProduct) {
      await this.productQueue.add(JobName.CreateProduct, {
        ...newProduct,
      });
    }

    return newProduct;
  }

  public async searchEngineListProduct(getListProductDto: GetListProductDto) {
    const { search, page, limit } = getListProductDto;

    if (!search) {
      return this.getListProduct(getListProductDto);
    }

    const [data, total] = await this.redisService.getOrSet(
      `search_engine_list_products_${JSON.stringify(getListProductDto)}`,
      async () => {
        const results = await this.productsSearchService.search(
          getListProductDto,
        );

        const ids = results.map((result: any) => result.id);

        const cases = ids
          .map((id, index) => `WHEN product.id = ${id} THEN ${index}`)
          .join(' ');

        if (!ids.length) {
          return [];
        }

        const [data, total] = await Promise.all([
          this.productsRepository
            .createQueryBuilder('product')
            .where('product.id IN (:...ids)', { ids })
            .orderBy(`CASE ${cases} END`)
            .leftJoinAndSelect('product.tradeMark', 'tradeMark')
            .leftJoinAndSelect('product.category', 'category')
            .limit(limit)
            .offset((page - 1) * limit)
            .getMany(),
          this.productsRepository
            .createQueryBuilder('product')
            .where('product.id IN (:...ids)', { ids })
            .limit(limit)
            .offset((page - 1) * limit)
            .getCount(),
        ]);

        return [data, total];
      },
      1,
    );

    return {
      data,
      total,
    };
  }

  public async getListProduct(getListProductDto: GetListProductDto) {
    const { page, limit, search, ...filter } = getListProductDto;

    const [data, total] = await this.redisService.getOrSet(
      `get_list_product_${JSON.stringify(getListProductDto)}`,
      async () => {
        const [data, total] = await Promise.all([
          this.productsRepository.find({
            where: [
              search
                ? {
                    ...filter,
                    deletedAt: IsNull(),
                    slug: Like(`%${toSlug(search)}%`),
                  }
                : { ...filter, deletedAt: IsNull() },
            ],
            order: {
              views: Order.DESC,
              click: Order.DESC,
            },
            skip: (page - 1) * limit,
            take: limit,
          }),
          this.productsRepository.count({
            where: [
              search
                ? {
                    ...filter,
                    deletedAt: IsNull(),
                    slug: Like(`%${toSlug(search)}%`),
                  }
                : { ...filter, deletedAt: IsNull() },
            ],
          }),
        ]);

        return [data, total];
      },
      300,
    );

    return {
      data,
      total,
    };
  }

  public async getProductById(productId: number): Promise<ProductEntity> {
    return await this.redisService.getOrSet(
      `${productId}`,
      async () => {
        return await this.productsRepository.findOne({
          where: [{ id: productId, deletedAt: IsNull() }],
          relations: ['medias', 'properties', 'category', 'tradeMark'],
        });
      },
      300,
    );
  }

  public async updateProductById(
    userId: number,
    productId: number,
    updateProductDto: UpdateProductDto,
  ): Promise<ProductEntity> {
    // eslint-disable-next-line prefer-const
    let { title } = updateProductDto;

    const product = await this.productsRepository.findOne({
      where: [{ id: productId, deletedAt: IsNull() }],
      relations: ['medias', 'properties', 'category', 'tradeMark'],
    });

    if (!product) throw new NotFoundException();

    let slug: string;

    if (title) {
      slug = toSlug(title);
    }

    const data = await this.productsRepository.save({
      id: productId,
      ...product,
      ...updateProductDto,
      updatedById: userId,
      slug,
    });

    if (data) await this.redisService.del(`${productId}`);

    return data;
  }

  public async deleteProduct(userId: number, productId: number) {
    const product = await this.productsRepository.findOne({
      where: [
        {
          id: productId,
          deletedAt: IsNull(),
        },
      ],
    });

    if (!product) throw new NotFoundException();

    return await this.productsRepository.save({
      ...product,
      id: productId,
      deletedById: userId,
      deletedAt: new Date(),
    });
  }
}
