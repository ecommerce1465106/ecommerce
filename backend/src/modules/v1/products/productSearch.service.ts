import { ProductSearchResult } from './../elasticsearch/interfaces/product-search.interface';
import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { GetListProductDto } from './dto/get-list-product.dto';
import { IProduct } from './interfaces/product.interfacr';

@Injectable()
export class ProductsSearchService {
  index = 'products';

  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexProduct(product: IProduct) {
    return this.elasticsearchService.index({
      index: this.index,
      body: {
        id: product.id,
        title: product.title.toLowerCase(),
        slug: product.slug.toLowerCase(),
        desc: product.desc.toLowerCase(),
        category: product.category.toLowerCase(),
        supplier: product.supplier.toLowerCase(),
        tradeMark: product.tradeMark.toLowerCase(),
      },
    });
  }

  async search(getListProductDto: GetListProductDto) {
    const data = await this.elasticsearchService.search<ProductSearchResult>({
      index: this.index,
      body: {
        query: {
          bool: {
            should: [
              {
                prefix: {
                  title: getListProductDto.search.toLowerCase(),
                },
              },
              {
                multi_match: {
                  query: getListProductDto.search,
                  fields: [
                    'title',
                    'slug',
                    'desc',
                    'supplier',
                    'category',
                    'tradeMark',
                  ],
                },
              },
            ],
          },
        },
        sort: [
          {
            _score: {
              order: 'desc',
            },
          },
        ],
        // from: (getListProductDto.page - 1) * getListProductDto.limit,
        // size: getListProductDto.limit,
      },
    });

    const hits = data.hits.hits;
    return hits.map((item: any) => item._source);
  }
}
