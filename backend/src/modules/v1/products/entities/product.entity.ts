import { TimeColumns } from '@root/models/common/time-columns';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { UserEntity } from '@root/modules/v1/users/entities/user.entity';
import { ProductMediaEntity } from './product-media.entity';
import { ProductCategoriesEntity } from '../../categories/entities/product-categories.entity';
import { ProductPropertiesEntity } from './product-properties.entity';
import { ProductTradeMarksEntity } from '../../trademark/entities/product-trademark.entity';
import { Supplier } from '@root/config/enum/supplier.enum';
import { Exclude } from 'class-transformer';

@Entity('products')
export class ProductEntity extends TimeColumns {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @Column()
  userId: number;

  @Column({ nullable: true })
  categoryId: number;

  @Column({ nullable: true })
  tradeMarkId: number;

  @Column()
  title: string;

  @Column()
  slug: string;

  @Column()
  desc: string;

  @Column()
  amount: number;

  @Column()
  thumnail: string;

  @Column({ default: 0 })
  views: number;

  @Column()
  shortLink: string;

  @Column({ default: true })
  isPublished: boolean;

  @Column({ select: false })
  @Exclude()
  rootLink: string;

  @Column({ enum: Supplier, default: Supplier.shopee })
  supplier: Supplier;

  @Column({ default: 0 })
  @Exclude()
  click: number;

  @OneToMany(() => ProductMediaEntity, (productImage) => productImage.product)
  medias: ProductMediaEntity[];

  @OneToMany(
    () => ProductPropertiesEntity,
    (productProperty) => productProperty.product,
  )
  properties: ProductPropertiesEntity[];

  @ManyToOne(() => ProductCategoriesEntity, (p_property) => p_property.products)
  category: ProductCategoriesEntity;

  @ManyToOne(() => ProductTradeMarksEntity, (p_property) => p_property.products)
  tradeMark: ProductTradeMarksEntity;

  @ManyToOne(() => UserEntity, (user) => user.tokens)
  user: UserEntity;
}
