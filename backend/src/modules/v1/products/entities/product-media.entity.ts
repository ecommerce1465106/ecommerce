import { MediaType } from '@root/config/enum/media-type.enum';
import { Exclude } from 'class-transformer';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ProductEntity } from './product.entity';

@Entity('p_media')
export class ProductMediaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  imageUrl: string;

  @Column({ nullable: true })
  @Exclude()
  productId: number;

  @Column({ nullable: true })
  type: MediaType;

  @Column({ nullable: true })
  thumnail: string;

  @ManyToOne(() => ProductEntity, (product) => product.medias, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @Exclude()
  product: ProductEntity;
}
