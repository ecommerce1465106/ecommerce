import { Exclude } from 'class-transformer';
import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProductEntity } from './product.entity';

@Entity('p_properties')
@Index(['p_k'])
@Index(['p_v'])
@Index(['p_v', 'p_k'])
export class ProductPropertiesEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  productId: number;

  @Column()
  p_k: string;

  @Column()
  p_v: string;

  @ManyToOne(() => ProductEntity, (product) => product.properties, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @Exclude()
  product: ProductEntity;
}
