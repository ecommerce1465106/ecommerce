import { TimeColumns } from '@root/models/common/time-columns';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  Index,
} from 'typeorm';
import { UserEntity } from '@root/modules/v1/users/entities/user.entity';

@Entity('tokens')
@Index(['token'])
@Index(['id', 'deletedAt'], { unique: true })
@Index(['ipAddress', 'deletedAt'], { unique: true })
export class TokenEntity extends TimeColumns {
  @PrimaryGeneratedColumn()
  public readonly id!: number;

  @Column()
  userId: number;

  @Column()
  token: string;

  @Column()
  refreshToken: string;

  @Column()
  expiresAt: Date;

  @Column()
  ipAddress: string;

  @Column()
  userAgent: string;

  @Column({ default: false })
  isBlackList: boolean;

  @ManyToOne(() => UserEntity, (user) => user.tokens)
  user: UserEntity;
}
