import { Role } from '@root/config/enum/role.enum';

export interface IJwtPayload {
  id: number;
  email: string;
  role: Role;
  permissions: any;
}
