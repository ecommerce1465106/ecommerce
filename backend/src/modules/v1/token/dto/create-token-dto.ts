import { IsDate, IsString } from 'class-validator';

export class CreateTokenDto {
  @IsString()
  token: string;

  @IsString()
  refreshToken: string;

  @IsDate()
  expiresAt: Date;
}
