import { RedisService } from '@root/common/utils/cache.util';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as moment from 'moment';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { TokenEntity } from './entities/token.entity';
import { IsNull, Repository } from 'typeorm';
import { Role } from '@root/config/enum/role.enum';

@Injectable()
export class TokensService {
  constructor(
    @InjectRepository(TokenEntity)
    private readonly tokensRepository: Repository<TokenEntity>,

    private readonly configService: ConfigService,

    private readonly jwtService: JwtService,

    private readonly redisServer: RedisService,
  ) {}

  public signAccessToken<T extends object>(payload: T): [string, Date] {
    const tokenExpiration = moment()
      .add(this.configService.get('jwt.access_token_ttl'), 'seconds')
      .toDate();
    const token = this.jwtService.sign(payload, {
      secret: this.configService.get('jwt.access_token_secret'),
      expiresIn: this.configService.get('jwt.access_token_ttl'),
    });

    return [token, tokenExpiration];
  }

  public signRefreshToken<T extends object>(payload: T): [string, Date] {
    const tokenExpiration = moment()
      .add(this.configService.get('jwt.refresh_token_secret'), 'seconds')
      .toDate();
    const token = this.jwtService.sign(payload, {
      secret: this.configService.get('jwt.refresh_token_secret'),
      expiresIn: this.configService.get('jwt.refresh_token_ttl'),
    });
    return [token, tokenExpiration];
  }

  public verifyRefreshToken(token: string) {
    const { id, email, role } = this.jwtService.verify(token, {
      secret: this.configService.get('jwt.refresh_token_secret'),
    });
    return { id, email, role } as {
      id: number;
      email: string;
      role: Role;
    };
  }

  public async createToken(
    token: string,
    refreshToken: string,
    expiresAt: Date,
    userId: number,
    ipAddress: string,
    userAgent: string,
  ) {
    return await this.tokensRepository.save({
      token,
      refreshToken,
      expiresAt,
      userId,
      ipAddress,
      userAgent,
      isBackList: false,
    });
  }

  public async updateToken(filter: object, data: object) {
    return await this.tokensRepository.update(filter, data);
  }

  public async getToken(token: string) {
    return this.redisServer.getOrSet(
      token,
      async () => {
        return await this.tokensRepository.findOne({
          where: [{ token, deletedAt: IsNull() }],
        });
      },
      30,
    );
  }

  public async refreshToken(
    token: string,
    ipAddress: string,
    userAgent: string,
  ) {
    const data: TokenEntity = await this.tokensRepository.findOne({
      where: [{ token, deletedAt: IsNull() }],
    });

    if (data?.ipAddress !== ipAddress || data?.userAgent !== userAgent) {
      await this.updateToken(
        { ipAddress, deletedAt: IsNull() },
        { isBlackList: true },
      );
      throw new UnauthorizedException();
    }

    if (data?.isBlackList || data?.deletedAt) {
      throw new UnauthorizedException();
    }

    const dataRefreshToken = this.verifyRefreshToken(data.refreshToken);

    if (!dataRefreshToken) {
      throw new UnauthorizedException();
    }

    const [access_token] = this.signAccessToken(dataRefreshToken);
    const [refresh_token, token_expired_time] =
      this.signRefreshToken(dataRefreshToken);

    await this.updateToken(
      { userId: dataRefreshToken.id, deletedAt: IsNull() },
      { deletedAt: new Date() },
    );

    const dataToken = await this.createToken(
      access_token,
      refresh_token,
      token_expired_time,
      dataRefreshToken.id,
      ipAddress,
      userAgent,
    );

    return {
      access_token: dataToken.token,
      expiredAt: token_expired_time.toISOString(),
      refresh_token: dataToken.refreshToken,
    };
  }
}
