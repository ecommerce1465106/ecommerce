import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisService } from '@root/common/utils/cache.util';
import { TokenEntity } from '@root/modules/v1/token/entities/token.entity';
import { TokensService } from '@root/modules/v1/token/token.service';

@Module({
  imports: [TypeOrmModule.forFeature([TokenEntity])],
  providers: [TokensService, RedisService, JwtService],
  exports: [TokensService],
})
export class TokensModule {}
