import { CreateTradeMarkDto } from './dto/create-trademark.dto';
import { Controller, Get, Body, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ProductTradeMarksEntity } from './entities/product-trademark.entity';
import { TradeMarksService } from './trademark.service';
import { ReqUser } from '@root/decorators/req-user.decorator';
import { Permissions } from '@root/config/enum/permission.enum';
import { Auth } from '@root/decorators/auth.decorator';
import { UseGuards } from '@nestjs/common/decorators';
import { JwtAuthGuard } from '@root/guards/jwt-auth.guard';
import { GetListTradeMarkDto } from './dto/get-list-trademark.dto';

@ApiTags('TradeMark')
@Controller('trade-mark')
export class TradeMarksController {
  constructor(private readonly tradeMarksService: TradeMarksService) {}

  // @Auth(Permissions.createTrademark)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Post()
  public async createTradeMark(
    @ReqUser() userId: number,
    @Body() createTrademarkDto: CreateTradeMarkDto,
  ): Promise<ProductTradeMarksEntity> {
    return await this.tradeMarksService.createTradeMark(
      userId,
      createTrademarkDto,
    );
  }

  @Get()
  public async getTradeMark(
    @Query() getListTradeMarkDto: GetListTradeMarkDto,
  ): Promise<ProductTradeMarksEntity[]> {
    return await this.tradeMarksService.getListTradeMark(getListTradeMarkDto);
  }
}
