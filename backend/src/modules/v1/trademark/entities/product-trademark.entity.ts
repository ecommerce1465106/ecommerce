import { TimeColumns } from '@root/models/common/time-columns';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ProductEntity } from '../../products/entities/product.entity';

@Entity('trademarks')
export class ProductTradeMarksEntity extends TimeColumns {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  avartar: string;

  @Column()
  desc: string;

  @Column({ default: 0 })
  startTotal: number;

  @OneToMany(() => ProductEntity, (product) => product.tradeMarkId)
  products: ProductEntity[];
}
