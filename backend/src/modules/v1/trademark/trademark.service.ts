import { CreateTradeMarkDto } from './dto/create-trademark.dto';
import { ProductTradeMarksEntity } from './entities/product-trademark.entity';
import { RedisService } from '@root/common/utils/cache.util';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, IsNull, Like } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { GetListTradeMarkDto } from './dto/get-list-trademark.dto';
import { Order } from '@root/config/enum/order-by.enum';

@Injectable()
export class TradeMarksService {
  constructor(
    @InjectRepository(ProductTradeMarksEntity)
    private readonly tradeMarksRepository: Repository<ProductTradeMarksEntity>,

    private readonly redisService: RedisService,
  ) {}

  public async createTradeMark(
    userId: number,
    createTradmarkDto: CreateTradeMarkDto,
  ): Promise<ProductTradeMarksEntity> {
    const { ...data } = createTradmarkDto;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [_, trademark] = await Promise.all([
      this.redisService.del('tradeMark'),
      this.tradeMarksRepository.save({
        ...data,
        createdById: userId,
      }),
    ]);

    return trademark;
  }

  public async getListTradeMark(
    getListTradeMarkDto: GetListTradeMarkDto,
  ): Promise<any> {
    const { page, limit, search } = getListTradeMarkDto;

    const [data, total] = await this.redisService.getOrSet(
      `tradeMark ${JSON.stringify(getListTradeMarkDto)}`,
      async () => {
        const [data, total] = await Promise.all([
          this.tradeMarksRepository.find({
            where: [
              search
                ? { deletedAt: IsNull(), name: Like(`%${search}%`) }
                : { deletedAt: IsNull() },
            ],
            order: {
              startTotal: Order.DESC,
            },
            skip: (page - 1) * limit,
            take: limit,
          }),
          this.tradeMarksRepository.count({
            where: [
              search
                ? { deletedAt: IsNull(), name: Like(`%${search}%`) }
                : { deletedAt: IsNull() },
            ],
          }),
        ]);

        return [data, total];
      },
      1,
    );

    console.log({ data });

    return { data, total };
  }

  public async getTradeMarkById(
    trademarkId: number,
  ): Promise<ProductTradeMarksEntity> {
    return await this.redisService.getOrSet(
      `${trademarkId}`,
      async () => {
        return await this.tradeMarksRepository.findOne({
          where: [{ id: trademarkId, deletedAt: IsNull() }],
        });
      },
      300,
    );
  }
}
