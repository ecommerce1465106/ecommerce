import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateTradeMarkDto {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  avartar: string;

  @ApiProperty()
  @IsString()
  desc: string;
}
