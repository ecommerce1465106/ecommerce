import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisService } from '@root/common/utils/cache.util';
import { ProductTradeMarksEntity } from './entities/product-trademark.entity';
import { TradeMarksController } from './trademark.controller';
import { TradeMarksService } from './trademark.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProductTradeMarksEntity])],
  controllers: [TradeMarksController],
  providers: [TradeMarksService, RedisService],
  exports: [TradeMarksService],
})
export class TradeMarksModule {}
